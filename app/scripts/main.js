$(document).ready(function () {

  new WOW().init();

  $('a[href^="#"]').on('click', function (e) {
    e.preventDefault();
    var target = this.hash,
      $target = $(target);

    $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  });

  $('.phone').inputmask('+7(999)999-99-99');

  $('.m-header__search').on('click', function (e) {
    e.preventDefault();
    $('.m-header__searching').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.m-modal__wrapper').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.close-modal').on('click', function (e) {
    e.preventDefault();
    $('.m-modal__wrapper').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.m-modal__wrapper').click(function (e) {
    if (!$(e.target).closest('.m-modal').length && !$(e.target).is('.m-modal')) {
      $('.m-modal__wrapper').slideToggle('fast', function (e) {
        // callback
      });
    }
  });

  $('.m-header__mob-nav').on('click', function (e) {
    e.preventDefault();
    $('.m-header__nav').slideToggle('fast', function (e) {
      // callback
    });
  });

  $('.m-good__collapse-header').on('click', function (e) {

    if (!$(e.target).closest('.m-good__collapse-button').length && !$(e.target).is('.m-good__collapse-button')) {
      $(this).next().slideToggle('fast', function (e) {
        $(this).prev().toggleClass('m-good__collapse-header_opened');
      });
    }
  });

  $('.m-clients__cards').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      }
    ]
  });

  $('.m-clients__prev').on('click', function () {
    $('.m-clients__cards').slick('slickPrev');
  });

  $('.m-clients__next').on('click', function () {
    $('.m-clients__cards').slick('slickNext');
  });

  $('.m-cert__cards').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: false
        }
      }
    ]
  });

  $('.m-cert__prev').on('click', function () {
    $('.m-cert__cards').slick('slickPrev');
  });

  $('.m-cert__next').on('click', function () {
    $('.m-cert__cards').slick('slickNext');
  });

});

